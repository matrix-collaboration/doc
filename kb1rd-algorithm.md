# What is this?
This is an attempt to create an algorithm similar to a CRDT that can share data over the Matrix protocol (or any message-passing system for that matter). I hope that someday, this or a similar algorithm can be compiled this into a working demo of such a collaboration system as a proof-of-concept. I hope that this may at least be of use/interest to any person wishing to implement such an algorithm in the future. I should also note that this is not really based upon serious academic research or study; I have read a few things online about CRDTs and I have read a few thorough studies of them, but I'm no expert. If you'd like to build on anything here, feel free to submit issues/PRs. I'd really like to see what other people think since I really have no idea what I'm doing!

# Requirements
The algorithm must fulfill the following:
1. Require no centralized server
2. Should sit between an application and Matrix, such that the application can be using any message bus (not just Matrix) and any program can be used to open a document
3. Store more than just text documents and be *open* to new and existing forms of data being shared. This would mean that users can collaborate on just about anything: Rich text documents, diagrams, drawings, and even media, such as music
4. Should work with potentially untrusted users (assuming permissions are properly assigned)
5. Eventually, should support encryption

# Technical Background
(If you are familiar with the inner workings of collaboration systems, you can mostly skip this)

You may have heard of collaboration being done using CRDTs. [CRDT](https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type) stands for Conflict-free Replicated Data Type. If you aren't familiar with them, it might be helpful to read the Wikipedia article (linked above). These data structures are by definition capable of replicating a data type across a computer network and merging it such that it is "*always mathematically possible to resolve inconsistencies which might result,*" (Quote from the Wikipedia article) hence the "conflict-free" part of CRDT.
I started with no understanding of what a CRDT is and I formulated a system of collaboration similar to a CRDT. The issue with this was that it was not *conflict-free,* rather it seemed more conflict tolerant. Rather than seamlessly merge text together, it would tell the application about the conflict and have the application resolve it. This way, the algorithm was much more modular and could more easily satisfy requirement #3. However, it was impossible to keep the CRDT name: It's outside of the CRDT definition. So the algorithm below is *technically* [Optimistic Replication](https://en.wikipedia.org/wiki/Optimistic_replication), which is a broader category. So the algorithm gives up Consistency in the [CAP theorem.](https://en.wikipedia.org/wiki/CAP_theorem)

# The Algorithm
## Example 1
Let's first start with an operational transformation like system. Each message is composed of patches to the document. Now one could just make a "parent" field in the message to form a DAG, but this has a problem: Assume that two messages are sent at the same time, or at least without knowledge of each other. To merge these, knowledge of the exact state of the document when the edit was performed must be obtained to correctly merge the two edits together. One way to do this is to store tombstone events, but these could accumulate quite quickly. Here is an example of what I mean: (Note: The message IDs here are just for example so that I can refer to the messages as 0, 1, and 2)

Message 0:
```json
{
    "id": "0:a.net",
    "sender": "alice",
    "patches": [
        {
            "start": 0,
            "length": 0,
            "replace-with": "Hello World"
        }
    ]
}
```


Message 1:
```json
{
    "id": "1:b.net",
    "parents": [ "0:a.net" ],
    "sender": "bob",
    "patches": [
        {
            "start": 11,
            "length": 0,
            "replace-with": "!"
        }
    ]
}
```

Message 2:
```json
{
    "id": "2:c.net",
    "parents": [ "0:a.net" ],
    "sender": "charlie",
    "patches": [
        {
            "start": 5,
            "length": 1,
            "replace-with": " wonderful "
        }
    ]
}
```

Bob's editor would display the following: `Hello World!`

And Charlie's editor would display: `Hello wonderful World`

Both would have the problem of merging each other's edits. They have two options: The first is to walk back down through every single message and merge the state together. The second is to retain a record of every past state of the document and then "transform" the patch operations into the new location. This may be feasible in this small example, but larger documents would lead to easy lockups and DoS attack vectors. This leads me to example 2...

## Example 2
One possible solution to this problem (if one is to keep using operational transformation) is to define the positions relative to a fixed system. Consider something like Logoot. It isn't operational transformation, but it has a fixed position system, if you will. That is to say that any insertion or modification would be transmitted in a message that can be merged at any time regardless of the state of the document. In Logoot, this is because each edit has its own ID which automatically positions it between groups of characters, the position of which may have changed in the client's copy. In the case of OT, positions in the text are relative to the *sender's* document, which makes it difficult to merge. (Note that "fixed position system" is not the proper terminology and I'm totally making it up because I don't know what else to call it)

In the case of the example above, this would mean defining the position relative to the *parents'* position. In the example above, this would change nothing because the parent message was the only text in the document. However, consider this example:

Message 0:
```json
{
    "id": "0:a.net",
    "sender": "alice",
    "patches": [
        {
            "start": 0,
            "length": 0,
            "replace-with": "Hello"
        }
    ]
}
```


Message 1:
```json
{
    "id": "1:b.net",
    "parents": [ "0:a.net" ],
    "sender": "bob",
    "patches": [
        {
            "start": 5,
            "length": 0,
            "replace-with": " World"
        }
    ]
}
```

Message 2:
```json
{
    "id": "2:c.net",
    "parents": [ "1:b.net" ],
    "sender": "charlie",
    "patches": [
        {
            "start": 6,
            "length": 0,
            "replace-with": "!"
        }
    ]
}
```

Notice how the `!` is added at position 6, which is the position in the *parent* message. In the first example, in order to fix the conflict, the position in the parent message would be determined, which is far easier than tracking the state or walking down the entire message tree. There is still the potential for unresolved conflict here: Let's say two people inserted something at the same position in the same parent. Who's text goes first? One message would be received first and placed into the client's document. The next message would be received then marked as a conflict because there is already something in that position. This way, the algorithm has created a blend of sequence CRDT methods and traditional operational transformation methods.

# A brief note about multiple patches
In the examples provided above, the `patches` object is an array supporting multiple patches. In theory, it would be possible to split each patch up in seperate messages, but there is a reason to have it in an array. If there is a conflict, correct client behavior would be to mark the *entire* message as a conflict. Consider the following HTML document:
```html
<div id="test" style="color: red;">
    Hello World!
</div>
```
A HTML editor may want to change the element type, but it would create errors if only one tag was changed. So it could create a message with two patches, one for the first `div` and one for the `div` in the closing tag. That way, if there is a conflict on either of the two tags, neither will be replaced unless the other is. The message **must** be rejected or accepted as a whole.

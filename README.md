# Matrix Collaboration Doc

Documentation for potential collaboration systems

**kb1rd-algorithm.md** - A possible algorithm for collaboration via Matrix (or similar) by KB1RD


This is supposed to be a working draft of a potential system and your ideas are welcome.

**Feel free to submit PRs if you'd like to add to this!**
